#include <stdint.h>
#include "BigInt.cu"

// The number of Padding bytes
#define numPaddingBytes 10

// Define the ReverseBytes Method
__device__ uint64_t reverseBytes(uint64_t x);

/**
 * Record digest for the current round. Working variables a through hh are
 * stored in the proper words of the digest (dig).
 */
__device__ void recordDigest
	(uint32_t* dig,
	 uint64_t a,
	 uint64_t b,
	 uint64_t c,
	 uint64_t d,
	 uint64_t e,
	 uint64_t f,
	 uint64_t g,
	 uint64_t h) {
	uint64_t tmp;
	tmp = reverseBytes(a);
	dig[6] = (uint32_t)(tmp >> 32);
	dig[5] = (uint32_t)(tmp);
	tmp = reverseBytes(b);
	dig[4] = (uint32_t)(tmp >> 32);
	dig[3] = (uint32_t)(tmp);
	tmp = reverseBytes(c);
	dig[2] = (uint32_t)(tmp >> 32);
	dig[1] = (uint32_t)(tmp);
	tmp = reverseBytes(d);
	dig[0] = (uint32_t)(tmp >> 32);
}

#include "SHA3Base.cu"

