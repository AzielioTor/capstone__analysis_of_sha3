/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.rit.sha3;

import edu.rit.util.BigInt;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author azieliotor
 */
public class Main {
    // https://www.di-mgt.com.au/sha_testvectors.html
    // https://encode-decode.com/sha3-256-generator-online/
    
    public static void main(String[] args) throws NoSuchAlgorithmException {
        BigInt A = new BigInt(256);
        A.fromString("0");
        BigInt B = new BigInt(256);
        B.fromString("0");
        
        final int OUTPUT_LENGTH = 2048;
        
        BigInt[] C_0 = new BigInt[24];
        BigInt[] C_1 = new BigInt[24];
        BigInt[] C_2 = new BigInt[24];
        BigInt[] C_3 = new BigInt[24];
        BigInt[] C_4 = new BigInt[24];
        BigInt[] C_5 = new BigInt[24];
        
        for (int i = 0; i < 24; i++) {
            C_0[i] = new BigInt(224);
            C_1[i] = new BigInt(256);
            C_2[i] = new BigInt(384);
            C_3[i] = new BigInt(512);
            // SHAKE
            C_4[i] = new BigInt(OUTPUT_LENGTH);
            C_5[i] = new BigInt(OUTPUT_LENGTH);
        }
        
        SHA3Base[] shas = new SHA3Base[6];
        shas[0] = new SHA3_224();
        shas[1] = new SHA3_256();
        shas[2] = new SHA3_384();
        shas[3] = new SHA3_512();
        
        SHAKE128 shake128 = new SHAKE128();
        SHAKE256 shake256 = new SHAKE256(); // Used to take parameter 2048
        
//        shas[0].evaluate(A, B, C_0);
        shake128.evaluate(A, B, C_4);
        System.out.println("SHAKE128: " + C_4[23]);
        
        shake256.evaluate(A, B, C_5);
        System.out.println("SHAKE256: " + C_5[23]);
//        shas[2].evaluate(A, B, C_2);
//        shas[3].evaluate(A, B, C_3);
    }
}
