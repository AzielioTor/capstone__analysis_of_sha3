
#ifndef __SHA3BASE_CU__
#define __SHA3BASE_CU__

#include <stdint.h>
#include "BigInt.cu"

#define MAX_CSIZE 2688

// Round constants.
__constant__ uint64_t K [24] = {
    0x0000000000000001L, 0x0000000000008082L, 
    0x800000000000808AL, 0x8000000080008000L, 
    0x000000000000808BL, 0x0000000080000001L,
    0x8000000080008081L, 0x8000000000008009L, 
    0x000000000000008AL, 0x0000000000000088L, 
    0x0000000080008009L, 0x000000008000000AL,
    0x000000008000808BL, 0x800000000000008BL, 
    0x8000000000008089L, 0x8000000000008003L, 
    0x8000000000008002L, 0x8000000000000080L,
    0x000000000000800AL, 0x800000008000000AL, 
    0x8000000080008081L, 0x8000000000008080L, 
    0x0000000080000001L, 0x8000000080008008L
};

/**
 * Left-rotate functions.
 */
__device__ uint64_t ROTL_1 (uint64_t x) {
	return (x << 1) | (x >> 63);
}

__device__ uint64_t ROTL_2 (uint64_t x) {
	return (x << 2) | (x >> 62);
}

__device__ uint64_t ROTL_3 (uint64_t x) {
	return (x << 3) | (x >> 61);
}

__device__ uint64_t ROTL_6 (uint64_t x) {
	return (x << 6) | (x >> 58);
}

__device__ uint64_t ROTL_8 (uint64_t x) {
	return (x << 8) | (x >> 56);
}

__device__ uint64_t ROTL_10 (uint64_t x) {
	return (x << 10) | (x >> 54);
}

__device__ uint64_t ROTL_14 (uint64_t x) {
	return (x << 14) | (x >> 50);
}

__device__ uint64_t ROTL_15 (uint64_t x) {
	return (x << 15) | (x >> 49);
}

__device__ uint64_t ROTL_18 (uint64_t x) {
	return (x << 18) | (x >> 46);
}

__device__ uint64_t ROTL_20 (uint64_t x) {
	return (x << 20) | (x >> 44);
}

__device__ uint64_t ROTL_21 (uint64_t x) {
	return (x << 21) | (x >> 43);
}

__device__ uint64_t ROTL_25 (uint64_t x) {
	return (x << 25) | (x >> 39);
}

__device__ uint64_t ROTL_27 (uint64_t x) {
	return (x << 27) | (x >> 37);
}

__device__ uint64_t ROTL_28 (uint64_t x) {
	return (x << 28) | (x >> 36);
}

__device__ uint64_t ROTL_36 (uint64_t x) {
	return (x << 36) | (x >> 28);
}

__device__ uint64_t ROTL_39 (uint64_t x) {
	return (x << 39) | (x >> 25);
}

__device__ uint64_t ROTL_41 (uint64_t x) {
	return (x << 41) | (x >> 23);
}

__device__ uint64_t ROTL_43 (uint64_t x) {
	return (x << 43) | (x >> 21);
}

__device__ uint64_t ROTL_44 (uint64_t x) {
	return (x << 44) | (x >> 20);
}

__device__ uint64_t ROTL_45 (uint64_t x) {
	return (x << 45) | (x >> 19);
}

__device__ uint64_t ROTL_55 (uint64_t x) {
	return (x << 55) | (x >> 9);
}

__device__ uint64_t ROTL_56 (uint64_t x) {
	return (x << 56) | (x >> 8);
}

__device__ uint64_t ROTL_61 (uint64_t x) {
	return (x << 61) | (x >> 3);
}

__device__ uint64_t ROTL_62 (uint64_t x) {
	return (x << 62) | (x >> 2);
}

/**
 * Reverse the bytes of the given long
 */
__device__ uint64_t reverseBytes(uint64_t x) {
	return ((((x) >> 56) & 0x00000000000000FF) | 
		(((x) >> 40) & 0x000000000000FF00) |
		(((x) >> 24) & 0x0000000000FF0000) | 
		(((x) >>  8) & 0x00000000FF000000) |
		(((x) <<  8) & 0x000000FF00000000) | 
		(((x) << 24) & 0x0000FF0000000000) |
		(((x) << 40) & 0x00FF000000000000) | 
		(((x) << 56) & 0xFF00000000000000));
}

/**
 * Record digest for the current round. Working variables a through h are
 * stored in the proper words of the digest (dig).
 */
 __device__ int recordDigest (uint32_t* dig, uint64_t val, int index) {
    uint64_t tmp;
    tmp = reverseBytes(val);
    dig[index    ] = (uint32_t)(tmp >> 32);
    dig[index - 1] = (uint32_t)(tmp);
    return index - 2;
}

// __device__ int recordDigest (uint32_t* dig, uint64_t num);
__device__ void extraRound(uint32_t* dig, uint32_t roundNum, uint64_t* W, int numLongs, int indexNum);
__device__ void secondSHA3(uint32_t roundNum, uint64_t* W);

/**
 * Evaluate the cryptographic function SHA3
 */
__device__ void evaluate(int NA, int Asize, uint32_t* A,
 	                     int NB, int Bsize, uint32_t* B,
	                     int R, int Csize, uint32_t* C,
	                     int a, int b) {
	// State variables
	uint64_t W [25];
	int numLongs = Csize / 64;
	int indexNum = (numLongs * 2) - 1;
	numLongs = 2048 / 64;
	indexNum = (numLongs * 2) - 1;
	printf("$$$$$ TEST: numLongs: %d, indexNum: %d, Csize: %llx", numLongs, indexNum);
	// Instanciate the rate of the SHA3 state
		// Get data from A & B
	biUnpackLongBigEndian (Asize, A, W, 0);
	biUnpackLongBigEndian (Bsize, B, W, 4);
		// Fill rest with padding and capacity
	W[ 0] = reverseBytes(W[ 0]);
	W[ 1] = reverseBytes(W[ 1]);
	W[ 2] = reverseBytes(W[ 2]);
	W[ 3] = reverseBytes(W[ 3]);
	W[ 4] = reverseBytes(W[ 4]);
	W[ 5] = reverseBytes(W[ 5]);
	W[ 6] = reverseBytes(W[ 6]);
	W[ 7] = reverseBytes(W[ 7]);
    // Padding
    W[ 8] = 0x000000000000001fL;
    W[ 9] = 0x0000000000000000L;
    W[10] = 0x0000000000000000L;
    W[11] = 0x0000000000000000L;
    W[12] = 0x0000000000000000L;
    W[13] = 0x0000000000000000L;
    W[14] = 0x0000000000000000L;
    W[15] = 0x0000000000000000L;
    W[16] = 0x0000000000000000L;
    W[17] = 0x0000000000000000L;
    W[18] = 0x0000000000000000L;
    W[19] = 0x8000000000000000L;
    // Capacity
    W[20] = 0x0000000000000000L;
    W[21] = 0x0000000000000000L;
    W[22] = 0x0000000000000000L;
    W[23] = 0x0000000000000000L;

	// Local temporary variables.
	uint64_t A00, A01, A02, A03, A04;
	uint64_t A10, A11, A12, A13, A14;
	uint64_t A20, A21, A22, A23, A24;
	uint64_t A30, A31, A32, A33, A34;
	uint64_t A40, A41, A42, A43, A44;
	
	uint64_t B00, B01, B02, B03, B04;
	uint64_t B10, B11, B12, B13, B14;
	uint64_t B20, B21, B22, B23, B24;
	uint64_t B30, B31, B32, B33, B34;
	uint64_t B40, B41, B42, B43, B44;
	uint64_t C0, C1, C2, C3, C4;
	uint64_t D0, D1, D2, D3, D4;

	A00 = W[ 0];
	A10 = W[ 1];
	A20 = W[ 2];
	A30 = W[ 3];
	A40 = W[ 4];
	A01 = W[ 5];
	A11 = W[ 6];
	A21 = W[ 7];
	A31 = W[ 8];
	A41 = W[ 9];
	A02 = W[10];
	A12 = W[11];
	A22 = W[12];
	A32 = W[13];
	A42 = W[14];
	A03 = W[15];
	A13 = W[16];
	A23 = W[17];
	A33 = W[18];
	A43 = W[19];
	A04 = W[20];
	A14 = W[21];
	A24 = W[22];
	A34 = W[23];
	A44 = W[24];

	// Do 24 rounds.
	for (int roundNum = 0; roundNum < 24; roundNum++) {
		indexNum = (numLongs * 2) - 1;
		// Theta step mapping
		// xor A lanes
		C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
		C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
		C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
		C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
		C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
		// xor and rotate
		D0 = C3 ^ ROTL_1(C0);
		D1 = C4 ^ ROTL_1(C1);
		D2 = C0 ^ ROTL_1(C2);
		D3 = C1 ^ ROTL_1(C3);
		D4 = C2 ^ ROTL_1(C4);
		// More lane xoring
		A00 ^= D1;
		A01 ^= D1;
		A02 ^= D1;
		A03 ^= D1;
		A04 ^= D1;
		A10 ^= D2;
		A11 ^= D2;
		A12 ^= D2;
		A13 ^= D2;
		A14 ^= D2;
		A20 ^= D3;
		A21 ^= D3;
		A22 ^= D3;
		A23 ^= D3;
		A24 ^= D3;
		A30 ^= D4;
		A31 ^= D4;
		A32 ^= D4;
		A33 ^= D4;
		A34 ^= D4;
		A40 ^= D0;
		A41 ^= D0;
		A42 ^= D0;
		A43 ^= D0;
		A44 ^= D0;
		
		// rho step mapping & pi step mapping
		B00 = A00;
		B13 = ROTL_36(A01);
		B21 = ROTL_3(A02);
		B34 = ROTL_41(A03);
		B42 = ROTL_18(A04);
		B02 = ROTL_1(A10);
		B10 = ROTL_44(A11);
		B23 = ROTL_10(A12);
		B31 = ROTL_45(A13);
		B44 = ROTL_2(A14);
		B04 = ROTL_62(A20);
		B12 = ROTL_6(A21);
		B20 = ROTL_43(A22);
		B33 = ROTL_15(A23);
		B41 = ROTL_61(A24);
		B01 = ROTL_28(A30);
		B14 = ROTL_55(A31);
		B22 = ROTL_25(A32);
		B30 = ROTL_21(A33);
		B43 = ROTL_56(A34);
		B03 = ROTL_27(A40);
		B11 = ROTL_20(A41);
		B24 = ROTL_39(A42);
		B32 = ROTL_8(A43);
		B40 = ROTL_14(A44);
		
		// chi step mapping
		A00 = B00 ^ (~B10 & B20);
		A01 = B01 ^ (~B11 & B21);
		A02 = B02 ^ (~B12 & B22);
		A03 = B03 ^ (~B13 & B23);
		A04 = B04 ^ (~B14 & B24);
		A10 = B10 ^ (~B20 & B30);
		A11 = B11 ^ (~B21 & B31);
		A12 = B12 ^ (~B22 & B32);
		A13 = B13 ^ (~B23 & B33);
		A14 = B14 ^ (~B24 & B34);
		A20 = B20 ^ (~B30 & B40);
		A21 = B21 ^ (~B31 & B41);
		A22 = B22 ^ (~B32 & B42);
		A23 = B23 ^ (~B33 & B43);
		A24 = B24 ^ (~B34 & B44);
		A30 = B30 ^ (~B40 & B00);
		A31 = B31 ^ (~B41 & B01);
		A32 = B32 ^ (~B42 & B02);
		A33 = B33 ^ (~B43 & B03);
		A34 = B34 ^ (~B44 & B04);
		A40 = B40 ^ (~B00 & B10);
		A41 = B41 ^ (~B01 & B11);
		A42 = B42 ^ (~B02 & B12);
		A43 = B43 ^ (~B03 & B13);
		A44 = B44 ^ (~B04 & B14);
		// iota step mapping
		A00 = A00 ^ K[roundNum];
		
		// Record digest for this round.
		if(numLongs >=  1) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A00, indexNum);
		if(numLongs >=  2) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A10, indexNum);
		if(numLongs >=  3) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A20, indexNum);
		if(numLongs >=  4) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A30, indexNum);
		if(numLongs >=  5) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A40, indexNum);
		if(numLongs >=  6) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A01, indexNum);
		if(numLongs >=  7) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A11, indexNum);
		if(numLongs >=  8) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A21, indexNum);
		if(numLongs >=  9) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A31, indexNum);
		if(numLongs >= 10) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A41, indexNum);
		if(numLongs >= 11) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A02, indexNum);
		if(numLongs >= 12) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A12, indexNum);
		if(numLongs >= 13) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A22, indexNum);
		if(numLongs >= 14) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A32, indexNum);
		if(numLongs >= 15) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A42, indexNum);
		if(numLongs >= 16) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A03, indexNum);
		if(numLongs >= 17) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A13, indexNum);
		if(numLongs >= 18) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A23, indexNum);
		if(numLongs >= 19) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A33, indexNum);
		if(numLongs >= 20) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A43, indexNum);
		if(numLongs >= 21) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, roundNum), A04, indexNum);
		if(numLongs >= 22 && roundNum != 23)  {
			W[ 0] = A00;
			W[ 1] = A10;
			W[ 2] = A20;
			W[ 3] = A30;
			W[ 4] = A40;
			W[ 5] = A01;
			W[ 6] = A11;
			W[ 7] = A21;
			W[ 8] = A31;
			W[ 9] = A41;
			W[10] = A02;
			W[11] = A12;
			W[12] = A22;
			W[13] = A32;
			W[14] = A42;
			W[15] = A03;
			W[16] = A13;
			W[17] = A23;
			W[18] = A33;
			W[19] = A43;
			W[20] = A04;
			W[21] = A14;
			W[22] = A24;
			W[23] = A34;
			W[24] = A44;
			extraRound(biElem3D(Csize, C, NB, R, a, b, roundNum), roundNum, W, numLongs, indexNum);
		}
	} // End round for loop
	if(numLongs >= 22) {
            
		secondSHA3( 0, W);
		secondSHA3( 1, W);
		secondSHA3( 2, W);
		secondSHA3( 3, W);
		secondSHA3( 4, W);
		secondSHA3( 5, W);
		secondSHA3( 6, W);
		secondSHA3( 7, W);
		secondSHA3( 8, W);
		secondSHA3( 9, W);
		secondSHA3(10, W);
		secondSHA3(11, W);
		secondSHA3(12, W);
		secondSHA3(13, W);
		secondSHA3(14, W);
		secondSHA3(15, W);
		secondSHA3(16, W);
		secondSHA3(17, W);
		secondSHA3(18, W);
		secondSHA3(19, W);
		secondSHA3(20, W);
		secondSHA3(21, W);
		secondSHA3(22, W);
		secondSHA3(23, W);
		
		A00 = W[ 0];
		A10 = W[ 1];
		A20 = W[ 2];
		A30 = W[ 3];
		A40 = W[ 4];
		A01 = W[ 5];
		A11 = W[ 6];
		A21 = W[ 7];
		A31 = W[ 8];
		A41 = W[ 9];
		A02 = W[10];
		A12 = W[11];
		A22 = W[12];
		A32 = W[13];
		A42 = W[14];
		A03 = W[15];
		A13 = W[16];
		A23 = W[17];
		A33 = W[18];
		A43 = W[19];
		A04 = W[20];
		A14 = W[21];
		A24 = W[22];
		A34 = W[23];
		A44 = W[24];

		if(numLongs >= 22) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A00, indexNum);
		if(numLongs >= 23) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A10, indexNum);
		if(numLongs >= 24) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A20, indexNum);
		if(numLongs >= 25) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A30, indexNum);
		if(numLongs >= 26) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A40, indexNum);
		if(numLongs >= 27) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A01, indexNum);
		if(numLongs >= 28) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A11, indexNum);
		if(numLongs >= 29) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A21, indexNum);
		if(numLongs >= 30) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A31, indexNum);
		if(numLongs >= 31) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A41, indexNum);
		if(numLongs >= 32) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A02, indexNum);
		if(numLongs >= 33) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A12, indexNum);
		if(numLongs >= 34) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A22, indexNum);
		if(numLongs >= 35) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A32, indexNum);
		if(numLongs >= 36) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A42, indexNum);
		if(numLongs >= 37) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A03, indexNum);
		if(numLongs >= 38) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A13, indexNum);
		if(numLongs >= 39) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A23, indexNum);
		if(numLongs >= 40) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A33, indexNum);
		if(numLongs >= 41) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A43, indexNum);
		if(numLongs >= 42) indexNum = recordDigest(biElem3D(Csize, C, NB, R, a, b, 23), A04, indexNum);
	}
}

/**
 * Evaluate the cryptographic function SHA3
 */
 __device__ void secondSHA3(uint32_t roundNum, uint64_t* W) {
	 // Local temporary variables.
	uint64_t A00, A01, A02, A03, A04;
	uint64_t A10, A11, A12, A13, A14;
	uint64_t A20, A21, A22, A23, A24;
	uint64_t A30, A31, A32, A33, A34;
	uint64_t A40, A41, A42, A43, A44;
	// Local temporary variables.
	uint64_t B00, B01, B02, B03, B04;
	uint64_t B10, B11, B12, B13, B14;
	uint64_t B20, B21, B22, B23, B24;
	uint64_t B30, B31, B32, B33, B34;
	uint64_t B40, B41, B42, B43, B44;
	uint64_t C0, C1, C2, C3, C4;
	uint64_t D0, D1, D2, D3, D4;
	
	A00 = W[ 0];
	A10 = W[ 1];
	A20 = W[ 2];
	A30 = W[ 3];
	A40 = W[ 4];
	A01 = W[ 5];
	A11 = W[ 6];
	A21 = W[ 7];
	A31 = W[ 8];
	A41 = W[ 9];
	A02 = W[10];
	A12 = W[11];
	A22 = W[12];
	A32 = W[13];
	A42 = W[14];
	A03 = W[15];
	A13 = W[16];
	A23 = W[17];
	A33 = W[18];
	A43 = W[19];
	A04 = W[20];
	A14 = W[21];
	A24 = W[22];
	A34 = W[23];
	A44 = W[24];
	// Theta step mapping
	// xor A lanes
	C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
	C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
	C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
	C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
	C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
	// xor and rotate
	D0 = C3 ^ ROTL_1(C0);
	D1 = C4 ^ ROTL_1(C1);
	D2 = C0 ^ ROTL_1(C2);
	D3 = C1 ^ ROTL_1(C3);
	D4 = C2 ^ ROTL_1(C4);
	// More lane xoring
	A00 ^= D1;
	A01 ^= D1;
	A02 ^= D1;
	A03 ^= D1;
	A04 ^= D1;
	A10 ^= D2;
	A11 ^= D2;
	A12 ^= D2;
	A13 ^= D2;
	A14 ^= D2;
	A20 ^= D3;
	A21 ^= D3;
	A22 ^= D3;
	A23 ^= D3;
	A24 ^= D3;
	A30 ^= D4;
	A31 ^= D4;
	A32 ^= D4;
	A33 ^= D4;
	A34 ^= D4;
	A40 ^= D0;
	A41 ^= D0;
	A42 ^= D0;
	A43 ^= D0;
	A44 ^= D0;
	
	// rho step mapping & pi step mapping
	B00 = A00;
	B13 = ROTL_36(A01);
	B21 = ROTL_3(A02);
	B34 = ROTL_41(A03);
	B42 = ROTL_18(A04);
	B02 = ROTL_1(A10);
	B10 = ROTL_44(A11);
	B23 = ROTL_10(A12);
	B31 = ROTL_45(A13);
	B44 = ROTL_2(A14);
	B04 = ROTL_62(A20);
	B12 = ROTL_6(A21);
	B20 = ROTL_43(A22);
	B33 = ROTL_15(A23);
	B41 = ROTL_61(A24);
	B01 = ROTL_28(A30);
	B14 = ROTL_55(A31);
	B22 = ROTL_25(A32);
	B30 = ROTL_21(A33);
	B43 = ROTL_56(A34);
	B03 = ROTL_27(A40);
	B11 = ROTL_20(A41);
	B24 = ROTL_39(A42);
	B32 = ROTL_8(A43);
	B40 = ROTL_14(A44);
	
	// chi step mapping
	A00 = B00 ^ (~B10 & B20);
	A01 = B01 ^ (~B11 & B21);
	A02 = B02 ^ (~B12 & B22);
	A03 = B03 ^ (~B13 & B23);
	A04 = B04 ^ (~B14 & B24);
	A10 = B10 ^ (~B20 & B30);
	A11 = B11 ^ (~B21 & B31);
	A12 = B12 ^ (~B22 & B32);
	A13 = B13 ^ (~B23 & B33);
	A14 = B14 ^ (~B24 & B34);
	A20 = B20 ^ (~B30 & B40);
	A21 = B21 ^ (~B31 & B41);
	A22 = B22 ^ (~B32 & B42);
	A23 = B23 ^ (~B33 & B43);
	A24 = B24 ^ (~B34 & B44);
	A30 = B30 ^ (~B40 & B00);
	A31 = B31 ^ (~B41 & B01);
	A32 = B32 ^ (~B42 & B02);
	A33 = B33 ^ (~B43 & B03);
	A34 = B34 ^ (~B44 & B04);
	A40 = B40 ^ (~B00 & B10);
	A41 = B41 ^ (~B01 & B11);
	A42 = B42 ^ (~B02 & B12);
	A43 = B43 ^ (~B03 & B13);
	A44 = B44 ^ (~B04 & B14);
	// iota step mapping
	A00 = A00 ^ K[roundNum];

	W[ 0] = A00;
	W[ 1] = A10;
	W[ 2] = A20;
	W[ 3] = A30;
	W[ 4] = A40;
	W[ 5] = A01;
	W[ 6] = A11;
	W[ 7] = A21;
	W[ 8] = A31;
	W[ 9] = A41;
	W[10] = A02;
	W[11] = A12;
	W[12] = A22;
	W[13] = A32;
	W[14] = A42;
	W[15] = A03;
	W[16] = A13;
	W[17] = A23;
	W[18] = A33;
	W[19] = A43;
	W[20] = A04;
	W[21] = A14;
	W[22] = A24;
	W[23] = A34;
	W[24] = A44;
}

__device__ void extraRound(uint32_t* dig, uint32_t roundNum, uint64_t* W, int numLongs, int indexNum) {
	// Local temporary variables.
	uint64_t A00, A01, A02, A03, A04;
	uint64_t A10, A11, A12, A13, A14;
	uint64_t A20, A21, A22, A23, A24;
	uint64_t A30, A31, A32, A33, A34;
	uint64_t A40, A41, A42, A43, A44;
	// Local temporary variables.
	uint64_t B00, B01, B02, B03, B04;
	uint64_t B10, B11, B12, B13, B14;
	uint64_t B20, B21, B22, B23, B24;
	uint64_t B30, B31, B32, B33, B34;
	uint64_t B40, B41, B42, B43, B44;
	uint64_t C0, C1, C2, C3, C4;
	uint64_t D0, D1, D2, D3, D4;
	// Theta step mapping
	A00 = W[ 0];
	A10 = W[ 1];
	A20 = W[ 2];
	A30 = W[ 3];
	A40 = W[ 4];
	A01 = W[ 5];
	A11 = W[ 6];
	A21 = W[ 7];
	A31 = W[ 8];
	A41 = W[ 9];
	A02 = W[10];
	A12 = W[11];
	A22 = W[12];
	A32 = W[13];
	A42 = W[14];
	A03 = W[15];
	A13 = W[16];
	A23 = W[17];
	A33 = W[18];
	A43 = W[19];
	A04 = W[20];
	A14 = W[21];
	A24 = W[22];
	A34 = W[23];
	A44 = W[24];
	// Theta step mapping
	// xor A lanes
	C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
	C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
	C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
	C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
	C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
	// xor and rotate
	D0 = C3 ^ ROTL_1(C0);
	D1 = C4 ^ ROTL_1(C1);
	D2 = C0 ^ ROTL_1(C2);
	D3 = C1 ^ ROTL_1(C3);
	D4 = C2 ^ ROTL_1(C4);
	// More lane xoring
	A00 ^= D1;
	A01 ^= D1;
	A02 ^= D1;
	A03 ^= D1;
	A04 ^= D1;
	A10 ^= D2;
	A11 ^= D2;
	A12 ^= D2;
	A13 ^= D2;
	A14 ^= D2;
	A20 ^= D3;
	A21 ^= D3;
	A22 ^= D3;
	A23 ^= D3;
	A24 ^= D3;
	A30 ^= D4;
	A31 ^= D4;
	A32 ^= D4;
	A33 ^= D4;
	A34 ^= D4;
	A40 ^= D0;
	A41 ^= D0;
	A42 ^= D0;
	A43 ^= D0;
	A44 ^= D0;
	
	// rho step mapping & pi step mapping
	B00 = A00;
	B13 = ROTL_36(A01);
	B21 = ROTL_3(A02);
	B34 = ROTL_41(A03);
	B42 = ROTL_18(A04);
	B02 = ROTL_1(A10);
	B10 = ROTL_44(A11);
	B23 = ROTL_10(A12);
	B31 = ROTL_45(A13);
	B44 = ROTL_2(A14);
	B04 = ROTL_62(A20);
	B12 = ROTL_6(A21);
	B20 = ROTL_43(A22);
	B33 = ROTL_15(A23);
	B41 = ROTL_61(A24);
	B01 = ROTL_28(A30);
	B14 = ROTL_55(A31);
	B22 = ROTL_25(A32);
	B30 = ROTL_21(A33);
	B43 = ROTL_56(A34);
	B03 = ROTL_27(A40);
	B11 = ROTL_20(A41);
	B24 = ROTL_39(A42);
	B32 = ROTL_8(A43);
	B40 = ROTL_14(A44);
	
	// chi step mapping
	A00 = B00 ^ (~B10 & B20);
	A01 = B01 ^ (~B11 & B21);
	A02 = B02 ^ (~B12 & B22);
	A03 = B03 ^ (~B13 & B23);
	A04 = B04 ^ (~B14 & B24);
	A10 = B10 ^ (~B20 & B30);
	A11 = B11 ^ (~B21 & B31);
	A12 = B12 ^ (~B22 & B32);
	A13 = B13 ^ (~B23 & B33);
	A14 = B14 ^ (~B24 & B34);
	A20 = B20 ^ (~B30 & B40);
	A21 = B21 ^ (~B31 & B41);
	A22 = B22 ^ (~B32 & B42);
	A23 = B23 ^ (~B33 & B43);
	A24 = B24 ^ (~B34 & B44);
	A30 = B30 ^ (~B40 & B00);
	A31 = B31 ^ (~B41 & B01);
	A32 = B32 ^ (~B42 & B02);
	A33 = B33 ^ (~B43 & B03);
	A34 = B34 ^ (~B44 & B04);
	A40 = B40 ^ (~B00 & B10);
	A41 = B41 ^ (~B01 & B11);
	A42 = B42 ^ (~B02 & B12);
	A43 = B43 ^ (~B03 & B13);
	A44 = B44 ^ (~B04 & B14);
	// iota step mapping
	A00 = A00 ^ K[roundNum];
	// Saving resultant array
	if(numLongs >= 22) indexNum = recordDigest(dig, A00, indexNum);
	if(numLongs >= 23) indexNum = recordDigest(dig, A10, indexNum);
	if(numLongs >= 24) indexNum = recordDigest(dig, A20, indexNum);
	if(numLongs >= 25) indexNum = recordDigest(dig, A30, indexNum);
	if(numLongs >= 26) indexNum = recordDigest(dig, A40, indexNum);
	if(numLongs >= 27) indexNum = recordDigest(dig, A01, indexNum);
	if(numLongs >= 28) indexNum = recordDigest(dig, A11, indexNum);
	if(numLongs >= 29) indexNum = recordDigest(dig, A21, indexNum);
	if(numLongs >= 30) indexNum = recordDigest(dig, A31, indexNum);
	if(numLongs >= 31) indexNum = recordDigest(dig, A41, indexNum);
	if(numLongs >= 32) indexNum = recordDigest(dig, A02, indexNum);
	if(numLongs >= 33) indexNum = recordDigest(dig, A12, indexNum);
	if(numLongs >= 34) indexNum = recordDigest(dig, A22, indexNum);
	if(numLongs >= 35) indexNum = recordDigest(dig, A32, indexNum);
	if(numLongs >= 36) indexNum = recordDigest(dig, A42, indexNum);
	if(numLongs >= 37) indexNum = recordDigest(dig, A03, indexNum);
	if(numLongs >= 38) indexNum = recordDigest(dig, A13, indexNum);
	if(numLongs >= 39) indexNum = recordDigest(dig, A23, indexNum);
	if(numLongs >= 40) indexNum = recordDigest(dig, A33, indexNum);
	if(numLongs >= 41) indexNum = recordDigest(dig, A43, indexNum);
	if(numLongs >= 42) indexNum = recordDigest(dig, A04, indexNum);
}

#include "FunctionKernel.cu"

#endif
