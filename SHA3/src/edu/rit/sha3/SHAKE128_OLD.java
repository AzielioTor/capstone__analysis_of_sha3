
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.rit.sha3;

import edu.rit.crst.Function;
import edu.rit.util.BigInt;

/**
 *
 * @author Aziel Shaw
 * @version March 21, 2019
 */
public class SHAKE128_OLD extends Function {

    // How will we handle rounds since we're running twice?
    // 
    
    /**
     * Round constants
     */
    private static final long[] K = new long[]{
        0x0000000000000001L, 0x0000000000008082L, 0x800000000000808AL,
        0x8000000080008000L, 0x000000000000808BL, 0x0000000080000001L,
        0x8000000080008081L, 0x8000000000008009L, 0x000000000000008AL,
        0x0000000000000088L, 0x0000000080008009L, 0x000000008000000AL,
        0x000000008000808BL, 0x800000000000008BL, 0x8000000000008089L,
        0x8000000000008003L, 0x8000000000008002L, 0x8000000000000080L,
        0x000000000000800AL, 0x800000008000000AL, 0x8000000080008081L,
        0x8000000000008080L, 0x0000000080000001L, 0x8000000080008008L
    };
    
    private int OUTPUT_LENGTH;
    
    /**
     * Default Constructor
     * @param outputLength The length of the output Digest
     */
    public SHAKE128_OLD(int outputLength) {
        super();
        OUTPUT_LENGTH = outputLength;
    }
    
    /**
     * Returns the constructor
     * @return The constructor for SHAKE128
     */
    @Override
    public String constructor() {
        return "edu.rit.sha3.SHAKE128()";
    }
    
    /**
     * The module location for the CUDA version of this hash function
     * @return The module location for the CUDA version of this hash function
     */
    @Override
    protected String moduleName() {
        return "edu/rit/sha3/SHAKE128.ptx";
    }

    /**
     * Description of this hash function
     * @return Description of this hash function
     */
    @Override
    public String description() {
        return "SHAKE128 XOR hash function";
    }

    @Override
    public String A_description() {
        return "message bits 0-255";
    }

    @Override
    public int A_bitSize() {
        return 512;
    }

    @Override
    public String B_description() {
        return "message bits 256-511";
    }

    @Override
    public int B_bitSize() {
        return 512;
    }

    @Override
    public String C_description() {
        return "digest";
    }

    @Override
    public int C_bitSize() {
        return OUTPUT_LENGTH;
    }

    @Override
    public int rounds() {
        return 24;
    }

    // State variables
    long A00, A01, A02, A03, A04,
         A10, A11, A12, A13, A14,
         A20, A21, A22, A23, A24,
         A30, A31, A32, A33, A34,
         A40, A41, A42, A43, A44;
    
    @Override
    public void evaluate(BigInt A, BigInt B, BigInt[] C) {
        
        A00 = 0x000000000000001fL;
        A10 = 0x0000000000000000L;
        A20 = 0x0000000000000000L;
        A30 = 0x0000000000000000L;
        A40 = 0x0000000000000000L;
        A01 = 0x0000000000000000L;
        A11 = 0x0000000000000000L;
        A21 = 0x0000000000000000L;
        A31 = 0x0000000000000000L;
        A41 = 0x0000000000000000L;
        A02 = 0x0000000000000000L;
        A12 = 0x0000000000000000L;
        A22 = 0x0000000000000000L;
        A32 = 0x0000000000000000L;
        A42 = 0x0000000000000000L;
        A03 = 0x0000000000000000L;
        A13 = 0x0000000000000000L;
        A23 = 0x0000000000000000L;
        A33 = 0x0000000000000000L;
        A43 = 0x0000000000000000L;
        A04 = 0x8000000000000000L;
        A14 = 0x0000000000000000L;
        A24 = 0x0000000000000000L;
        A34 = 0x0000000000000000L;
        A44 = 0x0000000000000000L;
        
        // Local temporary variables.
        long B00, B01, B02, B03, B04;
        long B10, B11, B12, B13, B14;
        long B20, B21, B22, B23, B24;
        long B30, B31, B32, B33, B34;
        long B40, B41, B42, B43, B44;
        long C0, C1, C2, C3, C4;
        long D0, D1, D2, D3, D4;
        
        // Iterate over 24 rounds
        for (int roundNum = 0; roundNum < 24; roundNum++) {
            // Theta step mapping
            // xor A lanes
            C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
            C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
            C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
            C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
            C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
            // xor and rotate
            D0 = C3 ^ Long.rotateLeft(C0, 1);
            D1 = C4 ^ Long.rotateLeft(C1, 1);
            D2 = C0 ^ Long.rotateLeft(C2, 1);
            D3 = C1 ^ Long.rotateLeft(C3, 1);
            D4 = C2 ^ Long.rotateLeft(C4, 1);
            // More lane xoring
            A00 ^= D1;
            A01 ^= D1;
            A02 ^= D1;
            A03 ^= D1;
            A04 ^= D1;
            A10 ^= D2;
            A11 ^= D2;
            A12 ^= D2;
            A13 ^= D2;
            A14 ^= D2;
            A20 ^= D3;
            A21 ^= D3;
            A22 ^= D3;
            A23 ^= D3;
            A24 ^= D3;
            A30 ^= D4;
            A31 ^= D4;
            A32 ^= D4;
            A33 ^= D4;
            A34 ^= D4;
            A40 ^= D0;
            A41 ^= D0;
            A42 ^= D0;
            A43 ^= D0;
            A44 ^= D0;
            
            // rho step mapping & pi step mapping
            B00 = A00;
            B13 = Long.rotateLeft(A01, 36);
            B21 = Long.rotateLeft(A02, 3);
            B34 = Long.rotateLeft(A03, 41);
            B42 = Long.rotateLeft(A04, 18);
            B02 = Long.rotateLeft(A10, 1);
            B10 = Long.rotateLeft(A11, 44);
            B23 = Long.rotateLeft(A12, 10);
            B31 = Long.rotateLeft(A13, 45);
            B44 = Long.rotateLeft(A14, 2);
            B04 = Long.rotateLeft(A20, 62);
            B12 = Long.rotateLeft(A21, 6);
            B20 = Long.rotateLeft(A22, 43);
            B33 = Long.rotateLeft(A23, 15);
            B41 = Long.rotateLeft(A24, 61);
            B01 = Long.rotateLeft(A30, 28);
            B14 = Long.rotateLeft(A31, 55);
            B22 = Long.rotateLeft(A32, 25);
            B30 = Long.rotateLeft(A33, 21);
            B43 = Long.rotateLeft(A34, 56);
            B03 = Long.rotateLeft(A40, 27);
            B11 = Long.rotateLeft(A41, 20);
            B24 = Long.rotateLeft(A42, 39);
            B32 = Long.rotateLeft(A43, 8);
            B40 = Long.rotateLeft(A44, 14);
            
            // chi step mapping
            A00 = B00 ^ (~B10 & B20);
            A01 = B01 ^ (~B11 & B21);
            A02 = B02 ^ (~B12 & B22);
            A03 = B03 ^ (~B13 & B23);
            A04 = B04 ^ (~B14 & B24);
            A10 = B10 ^ (~B20 & B30);
            A11 = B11 ^ (~B21 & B31);
            A12 = B12 ^ (~B22 & B32);
            A13 = B13 ^ (~B23 & B33);
            A14 = B14 ^ (~B24 & B34);
            A20 = B20 ^ (~B30 & B40);
            A21 = B21 ^ (~B31 & B41);
            A22 = B22 ^ (~B32 & B42);
            A23 = B23 ^ (~B33 & B43);
            A24 = B24 ^ (~B34 & B44);
            A30 = B30 ^ (~B40 & B00);
            A31 = B31 ^ (~B41 & B01);
            A32 = B32 ^ (~B42 & B02);
            A33 = B33 ^ (~B43 & B03);
            A34 = B34 ^ (~B44 & B04);
            A40 = B40 ^ (~B00 & B10);
            A41 = B41 ^ (~B01 & B11);
            A42 = B42 ^ (~B02 & B12);
            A43 = B43 ^ (~B03 & B13);
            A44 = B44 ^ (~B04 & B14);
            // iota step mapping
            A00 = A00 ^ K[roundNum];
            // Saving resultant array
            recordDigest(C[roundNum],
                A00, A10, A20, A30, A40,
                A01, A11, A21, A31, A41,
                A02, A12, A22, A32, A42,
                A03, A13, A23, A33, A43,
                A04, A14, A24, A34, A44);
        } // End round for loop
        
        // Iterate over 24 rounds
        for (int roundNum = 0; roundNum < 24; roundNum++) {
            // Theta step mapping
            // xor A lanes
            C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
            C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
            C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
            C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
            C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
            // xor and rotate
            D0 = C3 ^ Long.rotateLeft(C0, 1);
            D1 = C4 ^ Long.rotateLeft(C1, 1);
            D2 = C0 ^ Long.rotateLeft(C2, 1);
            D3 = C1 ^ Long.rotateLeft(C3, 1);
            D4 = C2 ^ Long.rotateLeft(C4, 1);
            // More lane xoring
            A00 ^= D1;
            A01 ^= D1;
            A02 ^= D1;
            A03 ^= D1;
            A04 ^= D1;
            A10 ^= D2;
            A11 ^= D2;
            A12 ^= D2;
            A13 ^= D2;
            A14 ^= D2;
            A20 ^= D3;
            A21 ^= D3;
            A22 ^= D3;
            A23 ^= D3;
            A24 ^= D3;
            A30 ^= D4;
            A31 ^= D4;
            A32 ^= D4;
            A33 ^= D4;
            A34 ^= D4;
            A40 ^= D0;
            A41 ^= D0;
            A42 ^= D0;
            A43 ^= D0;
            A44 ^= D0;
            
            // rho step mapping & pi step mapping
            B00 = A00;
            B13 = Long.rotateLeft(A01, 36);
            B21 = Long.rotateLeft(A02, 3);
            B34 = Long.rotateLeft(A03, 41);
            B42 = Long.rotateLeft(A04, 18);
            B02 = Long.rotateLeft(A10, 1);
            B10 = Long.rotateLeft(A11, 44);
            B23 = Long.rotateLeft(A12, 10);
            B31 = Long.rotateLeft(A13, 45);
            B44 = Long.rotateLeft(A14, 2);
            B04 = Long.rotateLeft(A20, 62);
            B12 = Long.rotateLeft(A21, 6);
            B20 = Long.rotateLeft(A22, 43);
            B33 = Long.rotateLeft(A23, 15);
            B41 = Long.rotateLeft(A24, 61);
            B01 = Long.rotateLeft(A30, 28);
            B14 = Long.rotateLeft(A31, 55);
            B22 = Long.rotateLeft(A32, 25);
            B30 = Long.rotateLeft(A33, 21);
            B43 = Long.rotateLeft(A34, 56);
            B03 = Long.rotateLeft(A40, 27);
            B11 = Long.rotateLeft(A41, 20);
            B24 = Long.rotateLeft(A42, 39);
            B32 = Long.rotateLeft(A43, 8);
            B40 = Long.rotateLeft(A44, 14);
            
            // chi step mapping
            A00 = B00 ^ (~B10 & B20);
            A01 = B01 ^ (~B11 & B21);
            A02 = B02 ^ (~B12 & B22);
            A03 = B03 ^ (~B13 & B23);
            A04 = B04 ^ (~B14 & B24);
            A10 = B10 ^ (~B20 & B30);
            A11 = B11 ^ (~B21 & B31);
            A12 = B12 ^ (~B22 & B32);
            A13 = B13 ^ (~B23 & B33);
            A14 = B14 ^ (~B24 & B34);
            A20 = B20 ^ (~B30 & B40);
            A21 = B21 ^ (~B31 & B41);
            A22 = B22 ^ (~B32 & B42);
            A23 = B23 ^ (~B33 & B43);
            A24 = B24 ^ (~B34 & B44);
            A30 = B30 ^ (~B40 & B00);
            A31 = B31 ^ (~B41 & B01);
            A32 = B32 ^ (~B42 & B02);
            A33 = B33 ^ (~B43 & B03);
            A34 = B34 ^ (~B44 & B04);
            A40 = B40 ^ (~B00 & B10);
            A41 = B41 ^ (~B01 & B11);
            A42 = B42 ^ (~B02 & B12);
            A43 = B43 ^ (~B03 & B13);
            A44 = B44 ^ (~B04 & B14);
            // iota step mapping
            A00 = A00 ^ K[roundNum];
        } // End round for loop
        long tmp;
        tmp = Long.reverseBytes(A00);
        C[23].value[21] = (int)(tmp >> 32);
        C[23].value[20] = (int)(tmp);
        tmp = Long.reverseBytes(A10);
        C[23].value[19] = (int)(tmp >> 32);
        C[23].value[18] = (int)(tmp);
        tmp = Long.reverseBytes(A20);
        C[23].value[17] = (int)(tmp >> 32);
        C[23].value[16] = (int)(tmp);
        tmp = Long.reverseBytes(A30);
        C[23].value[15] = (int)(tmp >> 32);
        C[23].value[14] = (int)(tmp);
        tmp = Long.reverseBytes(A40);
        C[23].value[13] = (int)(tmp >> 32);
        C[23].value[12] = (int)(tmp);
        tmp = Long.reverseBytes(A01);
        C[23].value[11] = (int)(tmp >> 32);
        C[23].value[10] = (int)(tmp);
        tmp = Long.reverseBytes(A11);
        C[23].value[ 9] = (int)(tmp >> 32);
        C[23].value[ 8] = (int)(tmp);
        tmp = Long.reverseBytes(A21);
        C[23].value[ 7] = (int)(tmp >> 32);
        C[23].value[ 6] = (int)(tmp);
        tmp = Long.reverseBytes(A31);
        C[23].value[ 5] = (int)(tmp >> 32);
        C[23].value[ 4] = (int)(tmp);
        tmp = Long.reverseBytes(A41);
        C[23].value[ 3] = (int)(tmp >> 32);
        C[23].value[ 2] = (int)(tmp);
        tmp = Long.reverseBytes(A02);
        C[23].value[ 1] = (int)(tmp >> 32);
        C[23].value[ 0] = (int)(tmp);
    }

    /**
     * Records the Digest
     * @param dig The BigInt to record into
     * @param a a
     * @param b b
     * @param c c
     * @param d d
     * @param e e
     * @param f f
     * @param g g
     * @param h h
     * @param i i
     * @param j j
     * @param k k
     * @param l l
     * @param m m
     * @param n n
     * @param o o
     * @param p p
     * @param q q
     * @param r r
     * @param s s
     * @param t t
     * @param u u
     * @param v v
     * @param w w
     * @param x x
     * @param y y
     */
    protected void recordDigest(BigInt dig, 
            long a, long b, long c, long d, long e, 
            long f, long g, long h, long i, long j, 
            long k, long l, long m, long n, long o, 
            long p, long q, long r, long s, long t, 
            long u, long v, long w, long x, long y) {
        long tmp;
        tmp = Long.reverseBytes(a);
        dig.value[63] = (int)(tmp >> 32);
        dig.value[62] = (int)(tmp);
        tmp = Long.reverseBytes(b);
        dig.value[61] = (int)(tmp >> 32);
        dig.value[60] = (int)(tmp);
        tmp = Long.reverseBytes(c);
        dig.value[59] = (int)(tmp >> 32);
        dig.value[58] = (int)(tmp);
        tmp = Long.reverseBytes(d);
        dig.value[57] = (int)(tmp >> 32);
        dig.value[56] = (int)(tmp);
        tmp = Long.reverseBytes(e);
        dig.value[55] = (int)(tmp >> 32);
        dig.value[54] = (int)(tmp);
        tmp = Long.reverseBytes(f);
        dig.value[53] = (int)(tmp >> 32);
        dig.value[52] = (int)(tmp);
        tmp = Long.reverseBytes(g);
        dig.value[51] = (int)(tmp >> 32);
        dig.value[50] = (int)(tmp);
        tmp = Long.reverseBytes(h);
        dig.value[49] = (int)(tmp >> 32);
        dig.value[48] = (int)(tmp);

        // second
        tmp = Long.reverseBytes(i);
        dig.value[47] = (int)(tmp >> 32);
        dig.value[46] = (int)(tmp);
        tmp = Long.reverseBytes(j);
        dig.value[45] = (int)(tmp >> 32);
        dig.value[44] = (int)(tmp);
        tmp = Long.reverseBytes(k);
        dig.value[43] = (int)(tmp >> 32);
        dig.value[42] = (int)(tmp);
        tmp = Long.reverseBytes(l);
        dig.value[41] = (int)(tmp >> 32);
        dig.value[40] = (int)(tmp);
        tmp = Long.reverseBytes(m);
        dig.value[39] = (int)(tmp >> 32);
        dig.value[38] = (int)(tmp);
        tmp = Long.reverseBytes(n);
        dig.value[37] = (int)(tmp >> 32);
        dig.value[36] = (int)(tmp);
        tmp = Long.reverseBytes(o);
        dig.value[35] = (int)(tmp >> 32);
        dig.value[34] = (int)(tmp);
        tmp = Long.reverseBytes(p);
        dig.value[33] = (int)(tmp >> 32);
        dig.value[32] = (int)(tmp);
        // third
        tmp = Long.reverseBytes(q);
        dig.value[31] = (int)(tmp >> 32);
        dig.value[30] = (int)(tmp);
        tmp = Long.reverseBytes(r);
        dig.value[29] = (int)(tmp >> 32);
        dig.value[28] = (int)(tmp);
        tmp = Long.reverseBytes(s);
        dig.value[27] = (int)(tmp >> 32);
        dig.value[26] = (int)(tmp);
        tmp = Long.reverseBytes(t);
        dig.value[25] = (int)(tmp >> 32);
        dig.value[24] = (int)(tmp);
        tmp = Long.reverseBytes(u);
        dig.value[23] = (int)(tmp >> 32);
        dig.value[22] = (int)(tmp);
    }
    
//    /**
//     * Records the Digest
//     * @param dig The BigInt to record into
//     * @param a a
//     * @param b b
//     * @param c c
//     * @param d d
//     * @param e e
//     * @param f f
//     * @param g g
//     * @param h h
//     * @param i i
//     * @param j j
//     * @param k k
//     * @param l l
//     * @param m m
//     * @param n n
//     * @param o o
//     * @param p p
//     * @param q q
//     * @param r r
//     * @param s s
//     * @param t t
//     * @param u u
//     * @param v v
//     * @param w w
//     * @param x x
//     * @param y y
//     */
//    protected void recordDigest(BigInt dig, 
//            long a, long b, long c, long d, long e, 
//            long f, long g, long h, long i, long j, 
//            long k, long l, long m, long n, long o, 
//            long p, long q, long r, long s, long t, 
//            long u, long v, long w, long x, long y) {
//        long tmp;
//        tmp = Long.reverseBytes(a);
//        dig.value[63] = (int)(tmp >> 32);
//        dig.value[62] = (int)(tmp);
//        tmp = Long.reverseBytes(b);
//        dig.value[61] = (int)(tmp >> 32);
//        dig.value[60] = (int)(tmp);
//        tmp = Long.reverseBytes(c);
//        dig.value[59] = (int)(tmp >> 32);
//        dig.value[58] = (int)(tmp);
//        tmp = Long.reverseBytes(d);
//        dig.value[57] = (int)(tmp >> 32);
//        dig.value[56] = (int)(tmp);
//        tmp = Long.reverseBytes(e);
//        dig.value[55] = (int)(tmp >> 32);
//        dig.value[54] = (int)(tmp);
//        tmp = Long.reverseBytes(f);
//        dig.value[53] = (int)(tmp >> 32);
//        dig.value[54] = (int)(tmp);
//        tmp = Long.reverseBytes(g);
//        dig.value[53] = (int)(tmp >> 32);
//        dig.value[52] = (int)(tmp);
//        tmp = Long.reverseBytes(h);
//        dig.value[51] = (int)(tmp >> 32);
//        dig.value[50] = (int)(tmp);
//    }
    
}
