/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.rit.sha3;

import edu.rit.crst.Function;
import static edu.rit.pj2.Task.terminate;
import edu.rit.util.BigInt;


/**
 *
 * @author Aziel Shaw
 * @version March 21, 2019
 */
public class SHAKE256 extends Function {
    
    /**
     * Round constants
     */
    private static final long[] K = new long[]{
        0x0000000000000001L, 0x0000000000008082L, 0x800000000000808AL,
        0x8000000080008000L, 0x000000000000808BL, 0x0000000080000001L,
        0x8000000080008081L, 0x8000000000008009L, 0x000000000000008AL,
        0x0000000000000088L, 0x0000000080008009L, 0x000000008000000AL,
        0x000000008000808BL, 0x800000000000008BL, 0x8000000000008089L,
        0x8000000000008003L, 0x8000000000008002L, 0x8000000000000080L,
        0x000000000000800AL, 0x800000008000000AL, 0x8000000080008081L,
        0x8000000000008080L, 0x0000000080000001L, 0x8000000080008008L
    };
    
    private static final int MAX_CSIZE = 2176;
    private static int Csize;
    private static int numLongs;
    private static int indexNum;
    
    /**
     * Default Constructor
     * @param csize The length of the output Digest
     */
    public SHAKE256(int csize) {
        super();
        if(csize % 64 != 0 || csize > MAX_CSIZE) {
            usage();
        }
        Csize = csize;
        numLongs = Csize / 64;
        indexNum = (numLongs * 2) - 1;
    }
    
    public SHAKE256() {
        this(2048);
    }
    
    /**
     * Returns the constructor
     * @return The constructor for SHAKE256
     */
    @Override
    public String constructor() {
        return "edu.rit.sha3.SHAKE256()";
    }
    
    /**
     * The module location for the CUDA version of this hash function
     * @return The module location for the CUDA version of this hash function
     */
    @Override
    protected String moduleName() {
        return "edu/rit/sha3/SHAKE256.ptx";
    }

    /**
     * Description of this hash function
     * @return Description of this hash function
     */
    @Override
    public String description() {
        return "SHAKE128 XOR hash function";
    }

    @Override
    public String A_description() {
        return "message bits 0-255";
    }

    @Override
    public int A_bitSize() {
        return 256;
    }

    @Override
    public String B_description() {
        return "message bits 256-511";
    }

    @Override
    public int B_bitSize() {
        return 256;
    }

    @Override
    public String C_description() {
        return "digest";
    }

    @Override
    public int C_bitSize() {
        return this.Csize;
    }

    @Override
    public int rounds() {
        return 24;
    }

    // State variables
    long A00, A01, A02, A03, A04,
         A10, A11, A12, A13, A14,
         A20, A21, A22, A23, A24,
         A30, A31, A32, A33, A34,
         A40, A41, A42, A43, A44;
    
    @Override
    public void evaluate(BigInt A, BigInt B, BigInt[] C) {
        // Get long arrays from A and B
        long[] Aarr = new long[4];
        A.unpackBigEndian(Aarr);
        long[] Barr = new long[4];
        B.unpackBigEndian(Barr);
        
        // Instanciate state variables
        A00 = Long.reverseBytes(Aarr[0]); // A1
        A10 = Long.reverseBytes(Aarr[1]); // A2
        A20 = Long.reverseBytes(Aarr[2]); // A3
        A30 = Long.reverseBytes(Aarr[3]); // A4
        A40 = Long.reverseBytes(Barr[0]); // B1
        A01 = Long.reverseBytes(Barr[1]); // B2
        A11 = Long.reverseBytes(Barr[2]); // B3
        A21 = Long.reverseBytes(Barr[3]); // B4
        // PADDING
        A31 = 0x000000000000001fL;
        A41 = 0x0000000000000000L;
        A02 = 0x0000000000000000L;
        A12 = 0x0000000000000000L;
        A22 = 0x0000000000000000L;
        A32 = 0x0000000000000000L;
        A42 = 0x0000000000000000L;
        A03 = 0x0000000000000000L;
        A13 = 0x8000000000000000L;
        // CAPACITY
        A23 = 0x0000000000000000L;
        A33 = 0x0000000000000000L;
        A43 = 0x0000000000000000L;
        A04 = 0x0000000000000000L;
        A14 = 0x0000000000000000L;
        A24 = 0x0000000000000000L;
        A34 = 0x0000000000000000L;
        A44 = 0x0000000000000000L;
        
        // Local temporary variables.
        long B00, B01, B02, B03, B04;
        long B10, B11, B12, B13, B14;
        long B20, B21, B22, B23, B24;
        long B30, B31, B32, B33, B34;
        long B40, B41, B42, B43, B44;
        long C0, C1, C2, C3, C4;
        long D0, D1, D2, D3, D4;
        
        // Iterate over 24 rounds
        for (int roundNum = 0; roundNum < 24; roundNum++) {
            indexNum = (numLongs * 2) - 1;
            // Theta step mapping
            // xor A lanes
            C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
            C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
            C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
            C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
            C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
            // xor and rotate
            D0 = C3 ^ Long.rotateLeft(C0, 1);
            D1 = C4 ^ Long.rotateLeft(C1, 1);
            D2 = C0 ^ Long.rotateLeft(C2, 1);
            D3 = C1 ^ Long.rotateLeft(C3, 1);
            D4 = C2 ^ Long.rotateLeft(C4, 1);
            // More lane xoring
            A00 ^= D1;
            A01 ^= D1;
            A02 ^= D1;
            A03 ^= D1;
            A04 ^= D1;
            A10 ^= D2;
            A11 ^= D2;
            A12 ^= D2;
            A13 ^= D2;
            A14 ^= D2;
            A20 ^= D3;
            A21 ^= D3;
            A22 ^= D3;
            A23 ^= D3;
            A24 ^= D3;
            A30 ^= D4;
            A31 ^= D4;
            A32 ^= D4;
            A33 ^= D4;
            A34 ^= D4;
            A40 ^= D0;
            A41 ^= D0;
            A42 ^= D0;
            A43 ^= D0;
            A44 ^= D0;
            
            // rho step mapping & pi step mapping
            B00 = A00;
            B13 = Long.rotateLeft(A01, 36);
            B21 = Long.rotateLeft(A02, 3);
            B34 = Long.rotateLeft(A03, 41);
            B42 = Long.rotateLeft(A04, 18);
            B02 = Long.rotateLeft(A10, 1);
            B10 = Long.rotateLeft(A11, 44);
            B23 = Long.rotateLeft(A12, 10);
            B31 = Long.rotateLeft(A13, 45);
            B44 = Long.rotateLeft(A14, 2);
            B04 = Long.rotateLeft(A20, 62);
            B12 = Long.rotateLeft(A21, 6);
            B20 = Long.rotateLeft(A22, 43);
            B33 = Long.rotateLeft(A23, 15);
            B41 = Long.rotateLeft(A24, 61);
            B01 = Long.rotateLeft(A30, 28);
            B14 = Long.rotateLeft(A31, 55);
            B22 = Long.rotateLeft(A32, 25);
            B30 = Long.rotateLeft(A33, 21);
            B43 = Long.rotateLeft(A34, 56);
            B03 = Long.rotateLeft(A40, 27);
            B11 = Long.rotateLeft(A41, 20);
            B24 = Long.rotateLeft(A42, 39);
            B32 = Long.rotateLeft(A43, 8);
            B40 = Long.rotateLeft(A44, 14);
            
            // chi step mapping
            A00 = B00 ^ (~B10 & B20);
            A01 = B01 ^ (~B11 & B21);
            A02 = B02 ^ (~B12 & B22);
            A03 = B03 ^ (~B13 & B23);
            A04 = B04 ^ (~B14 & B24);
            A10 = B10 ^ (~B20 & B30);
            A11 = B11 ^ (~B21 & B31);
            A12 = B12 ^ (~B22 & B32);
            A13 = B13 ^ (~B23 & B33);
            A14 = B14 ^ (~B24 & B34);
            A20 = B20 ^ (~B30 & B40);
            A21 = B21 ^ (~B31 & B41);
            A22 = B22 ^ (~B32 & B42);
            A23 = B23 ^ (~B33 & B43);
            A24 = B24 ^ (~B34 & B44);
            A30 = B30 ^ (~B40 & B00);
            A31 = B31 ^ (~B41 & B01);
            A32 = B32 ^ (~B42 & B02);
            A33 = B33 ^ (~B43 & B03);
            A34 = B34 ^ (~B44 & B04);
            A40 = B40 ^ (~B00 & B10);
            A41 = B41 ^ (~B01 & B11);
            A42 = B42 ^ (~B02 & B12);
            A43 = B43 ^ (~B03 & B13);
            A44 = B44 ^ (~B04 & B14);
            // iota step mapping
            A00 = A00 ^ K[roundNum];
            // Saving resultant array
            if(numLongs >=  1) recordDigest(C[roundNum], A00);
            if(numLongs >=  2) recordDigest(C[roundNum], A10);
            if(numLongs >=  3) recordDigest(C[roundNum], A20);
            if(numLongs >=  4) recordDigest(C[roundNum], A30);
            if(numLongs >=  5) recordDigest(C[roundNum], A40);
            if(numLongs >=  6) recordDigest(C[roundNum], A01);
            if(numLongs >=  7) recordDigest(C[roundNum], A11);
            if(numLongs >=  8) recordDigest(C[roundNum], A21);
            if(numLongs >=  9) recordDigest(C[roundNum], A31);
            if(numLongs >= 10) recordDigest(C[roundNum], A41);
            if(numLongs >= 11) recordDigest(C[roundNum], A02);
            if(numLongs >= 12) recordDigest(C[roundNum], A12);
            if(numLongs >= 13) recordDigest(C[roundNum], A22);
            if(numLongs >= 14) recordDigest(C[roundNum], A32);
            if(numLongs >= 15) recordDigest(C[roundNum], A42);
            if(numLongs >= 16) recordDigest(C[roundNum], A03);
            if(numLongs >= 17) recordDigest(C[roundNum], A13);
            if(numLongs >= 18) recordDigest(C[roundNum], A23);
            if(numLongs >= 19) recordDigest(C[roundNum], A33);
            if(numLongs >= 20) recordDigest(C[roundNum], A43);
            if(numLongs >= 21) recordDigest(C[roundNum], A04);
            if(numLongs >= 22 && roundNum != 23) extraRound(C[roundNum], roundNum);
        } // End round for loop
        if(numLongs >= 22) {
            
            secondSHA3( 0);
            secondSHA3( 1);
            secondSHA3( 2);
            secondSHA3( 3);
            secondSHA3( 4);
            secondSHA3( 5);
            secondSHA3( 6);
            secondSHA3( 7);
            secondSHA3( 8);
            secondSHA3( 9);
            secondSHA3(10);
            secondSHA3(11);
            secondSHA3(12);
            secondSHA3(13);
            secondSHA3(14);
            secondSHA3(15);
            secondSHA3(16);
            secondSHA3(17);
            secondSHA3(18);
            secondSHA3(19);
            secondSHA3(20);
            secondSHA3(21);
            secondSHA3(22);
            secondSHA3(23);
            
            if(numLongs >= 22) recordDigest(C[23], A00);
            if(numLongs >= 23) recordDigest(C[23], A10);
            if(numLongs >= 24) recordDigest(C[23], A20);
            if(numLongs >= 25) recordDigest(C[23], A30);
            if(numLongs >= 26) recordDigest(C[23], A40);
            if(numLongs >= 27) recordDigest(C[23], A01);
            if(numLongs >= 28) recordDigest(C[23], A11);
            if(numLongs >= 29) recordDigest(C[23], A21);
            if(numLongs >= 30) recordDigest(C[23], A31);
            if(numLongs >= 31) recordDigest(C[23], A41);
            if(numLongs >= 32) recordDigest(C[23], A02);
            if(numLongs >= 33) recordDigest(C[23], A12);
            if(numLongs >= 34) recordDigest(C[23], A22);
        }
    }
    
    private void extraRound(BigInt C, int roundNum) {
        // Local temporary variables.
        long A00, A01, A02, A03, A04;
        long A10, A11, A12, A13, A14;
        long A20, A21, A22, A23, A24;
        long A30, A31, A32, A33, A34;
        long A40, A41, A42, A43, A44;
        long B00, B01, B02, B03, B04;
        long B10, B11, B12, B13, B14;
        long B20, B21, B22, B23, B24;
        long B30, B31, B32, B33, B34;
        long B40, B41, B42, B43, B44;
        long C0, C1, C2, C3, C4;
        long D0, D1, D2, D3, D4;
        // Theta step mapping
        A00 = this.A00;
        A10 = this.A10;
        A20 = this.A20;
        A30 = this.A30;
        A40 = this.A40;
        A01 = this.A01;
        A11 = this.A11;
        A21 = this.A21;
        A31 = this.A31;
        A41 = this.A41;
        A02 = this.A02;
        A12 = this.A12;
        A22 = this.A22;
        A32 = this.A32;
        A42 = this.A42;
        A03 = this.A03;
        A13 = this.A13;
        A23 = this.A23;
        A33 = this.A33;
        A43 = this.A43;
        A04 = this.A04;
        A14 = this.A14;
        A24 = this.A24;
        A34 = this.A34;
        A44 = this.A44;
        // xor A lanes
        C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
        C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
        C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
        C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
        C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
        // xor and rotate
        D0 = C3 ^ Long.rotateLeft(C0, 1);
        D1 = C4 ^ Long.rotateLeft(C1, 1);
        D2 = C0 ^ Long.rotateLeft(C2, 1);
        D3 = C1 ^ Long.rotateLeft(C3, 1);
        D4 = C2 ^ Long.rotateLeft(C4, 1);
        // More lane xoring
        A00 ^= D1;
        A01 ^= D1;
        A02 ^= D1;
        A03 ^= D1;
        A04 ^= D1;
        A10 ^= D2;
        A11 ^= D2;
        A12 ^= D2;
        A13 ^= D2;
        A14 ^= D2;
        A20 ^= D3;
        A21 ^= D3;
        A22 ^= D3;
        A23 ^= D3;
        A24 ^= D3;
        A30 ^= D4;
        A31 ^= D4;
        A32 ^= D4;
        A33 ^= D4;
        A34 ^= D4;
        A40 ^= D0;
        A41 ^= D0;
        A42 ^= D0;
        A43 ^= D0;
        A44 ^= D0;

        // rho step mapping & pi step mapping
        B00 = A00;
        B13 = Long.rotateLeft(A01, 36);
        B21 = Long.rotateLeft(A02, 3);
        B34 = Long.rotateLeft(A03, 41);
        B42 = Long.rotateLeft(A04, 18);
        B02 = Long.rotateLeft(A10, 1);
        B10 = Long.rotateLeft(A11, 44);
        B23 = Long.rotateLeft(A12, 10);
        B31 = Long.rotateLeft(A13, 45);
        B44 = Long.rotateLeft(A14, 2);
        B04 = Long.rotateLeft(A20, 62);
        B12 = Long.rotateLeft(A21, 6);
        B20 = Long.rotateLeft(A22, 43);
        B33 = Long.rotateLeft(A23, 15);
        B41 = Long.rotateLeft(A24, 61);
        B01 = Long.rotateLeft(A30, 28);
        B14 = Long.rotateLeft(A31, 55);
        B22 = Long.rotateLeft(A32, 25);
        B30 = Long.rotateLeft(A33, 21);
        B43 = Long.rotateLeft(A34, 56);
        B03 = Long.rotateLeft(A40, 27);
        B11 = Long.rotateLeft(A41, 20);
        B24 = Long.rotateLeft(A42, 39);
        B32 = Long.rotateLeft(A43, 8);
        B40 = Long.rotateLeft(A44, 14);

        // chi step mapping
        A00 = B00 ^ (~B10 & B20);
        A01 = B01 ^ (~B11 & B21);
        A02 = B02 ^ (~B12 & B22);
        A03 = B03 ^ (~B13 & B23);
        A04 = B04 ^ (~B14 & B24);
        A10 = B10 ^ (~B20 & B30);
        A11 = B11 ^ (~B21 & B31);
        A12 = B12 ^ (~B22 & B32);
        A13 = B13 ^ (~B23 & B33);
        A14 = B14 ^ (~B24 & B34);
        A20 = B20 ^ (~B30 & B40);
        A21 = B21 ^ (~B31 & B41);
        A22 = B22 ^ (~B32 & B42);
        A23 = B23 ^ (~B33 & B43);
        A24 = B24 ^ (~B34 & B44);
        A30 = B30 ^ (~B40 & B00);
        A31 = B31 ^ (~B41 & B01);
        A32 = B32 ^ (~B42 & B02);
        A33 = B33 ^ (~B43 & B03);
        A34 = B34 ^ (~B44 & B04);
        A40 = B40 ^ (~B00 & B10);
        A41 = B41 ^ (~B01 & B11);
        A42 = B42 ^ (~B02 & B12);
        A43 = B43 ^ (~B03 & B13);
        A44 = B44 ^ (~B04 & B14);
        // iota step mapping
        A00 = A00 ^ K[roundNum];
        // Saving resultant array
        if(numLongs >= 22) recordDigest(C, A00);
        if(numLongs >= 23) recordDigest(C, A10);
        if(numLongs >= 24) recordDigest(C, A20);
        if(numLongs >= 25) recordDigest(C, A30);
        if(numLongs >= 26) recordDigest(C, A40);
        if(numLongs >= 27) recordDigest(C, A01);
        if(numLongs >= 28) recordDigest(C, A11);
        if(numLongs >= 29) recordDigest(C, A21);
        if(numLongs >= 30) recordDigest(C, A31);
        if(numLongs >= 31) recordDigest(C, A41);
        if(numLongs >= 32) recordDigest(C, A02);
        if(numLongs >= 33) recordDigest(C, A12);
        if(numLongs >= 34) recordDigest(C, A22);
    }
    
    private void secondSHA3(int roundNum) {
        // Local temporary variables.
        long B00, B01, B02, B03, B04;
        long B10, B11, B12, B13, B14;
        long B20, B21, B22, B23, B24;
        long B30, B31, B32, B33, B34;
        long B40, B41, B42, B43, B44;
        long C0, C1, C2, C3, C4;
        long D0, D1, D2, D3, D4;
        
        // Theta step mapping
        // xor A lanes
        C0 = A00 ^ A01 ^ A02 ^ A03 ^ A04;
        C1 = A10 ^ A11 ^ A12 ^ A13 ^ A14;
        C2 = A20 ^ A21 ^ A22 ^ A23 ^ A24;
        C3 = A30 ^ A31 ^ A32 ^ A33 ^ A34;
        C4 = A40 ^ A41 ^ A42 ^ A43 ^ A44;
        // xor and rotate
        D0 = C3 ^ Long.rotateLeft(C0, 1);
        D1 = C4 ^ Long.rotateLeft(C1, 1);
        D2 = C0 ^ Long.rotateLeft(C2, 1);
        D3 = C1 ^ Long.rotateLeft(C3, 1);
        D4 = C2 ^ Long.rotateLeft(C4, 1);
        // More lane xoring
        A00 ^= D1;
        A01 ^= D1;
        A02 ^= D1;
        A03 ^= D1;
        A04 ^= D1;
        A10 ^= D2;
        A11 ^= D2;
        A12 ^= D2;
        A13 ^= D2;
        A14 ^= D2;
        A20 ^= D3;
        A21 ^= D3;
        A22 ^= D3;
        A23 ^= D3;
        A24 ^= D3;
        A30 ^= D4;
        A31 ^= D4;
        A32 ^= D4;
        A33 ^= D4;
        A34 ^= D4;
        A40 ^= D0;
        A41 ^= D0;
        A42 ^= D0;
        A43 ^= D0;
        A44 ^= D0;

        // rho step mapping & pi step mapping
        B00 = A00;
        B13 = Long.rotateLeft(A01, 36);
        B21 = Long.rotateLeft(A02, 3);
        B34 = Long.rotateLeft(A03, 41);
        B42 = Long.rotateLeft(A04, 18);
        B02 = Long.rotateLeft(A10, 1);
        B10 = Long.rotateLeft(A11, 44);
        B23 = Long.rotateLeft(A12, 10);
        B31 = Long.rotateLeft(A13, 45);
        B44 = Long.rotateLeft(A14, 2);
        B04 = Long.rotateLeft(A20, 62);
        B12 = Long.rotateLeft(A21, 6);
        B20 = Long.rotateLeft(A22, 43);
        B33 = Long.rotateLeft(A23, 15);
        B41 = Long.rotateLeft(A24, 61);
        B01 = Long.rotateLeft(A30, 28);
        B14 = Long.rotateLeft(A31, 55);
        B22 = Long.rotateLeft(A32, 25);
        B30 = Long.rotateLeft(A33, 21);
        B43 = Long.rotateLeft(A34, 56);
        B03 = Long.rotateLeft(A40, 27);
        B11 = Long.rotateLeft(A41, 20);
        B24 = Long.rotateLeft(A42, 39);
        B32 = Long.rotateLeft(A43, 8);
        B40 = Long.rotateLeft(A44, 14);

        // chi step mapping
        A00 = B00 ^ (~B10 & B20);
        A01 = B01 ^ (~B11 & B21);
        A02 = B02 ^ (~B12 & B22);
        A03 = B03 ^ (~B13 & B23);
        A04 = B04 ^ (~B14 & B24);
        A10 = B10 ^ (~B20 & B30);
        A11 = B11 ^ (~B21 & B31);
        A12 = B12 ^ (~B22 & B32);
        A13 = B13 ^ (~B23 & B33);
        A14 = B14 ^ (~B24 & B34);
        A20 = B20 ^ (~B30 & B40);
        A21 = B21 ^ (~B31 & B41);
        A22 = B22 ^ (~B32 & B42);
        A23 = B23 ^ (~B33 & B43);
        A24 = B24 ^ (~B34 & B44);
        A30 = B30 ^ (~B40 & B00);
        A31 = B31 ^ (~B41 & B01);
        A32 = B32 ^ (~B42 & B02);
        A33 = B33 ^ (~B43 & B03);
        A34 = B34 ^ (~B44 & B04);
        A40 = B40 ^ (~B00 & B10);
        A41 = B41 ^ (~B01 & B11);
        A42 = B42 ^ (~B02 & B12);
        A43 = B43 ^ (~B03 & B13);
        A44 = B44 ^ (~B04 & B14);
        // iota step mapping
        A00 = A00 ^ K[roundNum];
    }

    /**
     * Records the Digest
     * @param dig The BigInt to record into
     * @param num Number to add to Digest
     */
    protected void recordDigest(BigInt dig, long num) {
        int indexToAdd = indexNum;
        long tmp;
        tmp = Long.reverseBytes(num);
        dig.value[indexToAdd    ] = (int)(tmp >> 32);
        dig.value[indexToAdd - 1] = (int)(tmp);
        indexNum -= 2;
    }
    
    /**
     * Prints usage message and exits
     */
    public void usage() {
        System.out.println("Number passed in must be divisible by 64 and less than 2688");
        terminate(1);
    }
    
}
