#include <stdint.h>
#include "BigInt.cu"

// The number of Padding bytes
#define numPaddingBytes 1

// Define the ReverseBytes Method
__device__ uint64_t reverseBytes(uint64_t x);

/**
 * Record digest for the current round. Working variables a through h are
 * stored in the proper words of the digest (dig).
 */
__device__ void recordDigest
	(uint32_t* dig,
	 uint64_t a,
	 uint64_t b,
	 uint64_t c,
	 uint64_t d,
	 uint64_t e,
	 uint64_t f,
	 uint64_t g,
	 uint64_t h) {
	uint64_t tmp;
    tmp = reverseBytes(a);
    dig[15] = (uint32_t)(tmp >> 32);
    dig[14] = (uint32_t)(tmp);
    tmp = reverseBytes(b);
    dig[13] = (uint32_t)(tmp >> 32);
    dig[12] = (uint32_t)(tmp);
    tmp = reverseBytes(c);
    dig[11] = (uint32_t)(tmp >> 32);
    dig[10] = (uint32_t)(tmp);
    tmp = reverseBytes(d);
    dig[ 9] = (uint32_t)(tmp >> 32);
    dig[ 8] = (uint32_t)(tmp);
    tmp = reverseBytes(e);
    dig[ 7] = (uint32_t)(tmp >> 32);
    dig[ 6] = (uint32_t)(tmp);
    tmp = reverseBytes(f);
    dig[ 5] = (uint32_t)(tmp >> 32);
    dig[ 4] = (uint32_t)(tmp);
    tmp = reverseBytes(g);
    dig[ 3] = (uint32_t)(tmp >> 32);
    dig[ 2] = (uint32_t)(tmp);
    tmp = reverseBytes(h);
    dig[ 1] = (uint32_t)(tmp >> 32);
    dig[ 0] = (uint32_t)(tmp);
}

#include "SHA3Base.cu"

